package com.eclipse.springForm.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.eclipse.springForm.dao.PersonneRepository;
import com.eclipse.springForm.models.Personne;

@Controller
public class FormController {
	
	@Autowired
	private PersonneRepository personneRepository;
	@GetMapping("/personne")
	public String personneForm(Model model) {
	model.addAttribute("personne", new Personne());
	return "personneForm";
	}
	
	@PostMapping("/personne")
	public String personneSubmit(@ModelAttribute("personne") @Valid Personne personne, BindingResult result, Model model) 
	{
		if (result.hasErrors()) { return "personneForm"; } 
		
	Personne p1 = personneRepository.save(personne);
	model.addAttribute("personne",p1);
	return "success";
	}
}
